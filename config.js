module.exports = {
  "host": "localhost",
  "port": "9042",
  "user": "cassandra",
  "password": "cassandra",
  "localDataCenter": "datacenter1",
  "keyspace": "prod",
  "dbms": "cassandra",
  "format": "jsonSchema4",
  "output": "result.json"
};

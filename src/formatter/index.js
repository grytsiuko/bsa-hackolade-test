const formatters = require('./formatters');

const formatSchema = (data, config) => {
  const {format} = config;
  const formatter = formatters[format];

  if (!formatter) {
    throw new Error('Unknown format');
  }
  return formatter(data, config);
};

module.exports = formatSchema;

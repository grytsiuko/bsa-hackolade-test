const fs = require('fs');

const formatJsonSchema4 = (data, config) => {
  const output = config.output;
  const formattedData = formatData(data);
  writeJson(formattedData, output);
};

const formatData = data => {
  const tableEntries = data.tables.map(t => getTableEntry(t));
  const tablesData = Object.fromEntries(tableEntries);

  return {
    '$schema': 'http://json-schema.org/draft-04/schema#',
    title: data.dataBaseName,
    type: 'object',
    properties: tablesData
  };
};

const getTableEntry = table => {
  const columnEntries = table.columnTypes.map(t => getColumnEntry(t));
  const columnsData = Object.fromEntries(columnEntries);

  return [
    table.tableName,
    {type: 'object', properties: columnsData}
  ];
};

const getColumnEntry = tableTypes => ([
  tableTypes.columnName,
  tableTypes.description
]);

const writeJson = (data, filePath) => {
  try {
    const json = JSON.stringify(data, null, 2);
    fs.writeFile(filePath, json, () => {});
  } catch (e) {
    throw new Error('Unable to write to file');
  }
};

module.exports = formatJsonSchema4;


const parseDb = require('./parser');
const formatSchema = require('./formatter');

const convert = config => {
  return parseDb(config)
    .then(data => formatSchema(data, config))
    .then(() => console.log('Success'));
};

module.exports = convert;

const parsers = require('./parsers');

const parseDb = async config => {
  const {dbms} = config;
  const parser = parsers[dbms];

  if (parser === undefined) {
    throw new Error('Unknown DBMS');
  }
  return parser(config);
};

module.exports = parseDb;


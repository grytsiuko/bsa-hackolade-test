const typeCodes = require('./typeCodes');
const typeDescription = require('./typeDescription');

const parseColumn = column => {
  return {
    columnName: column.name,
    description: parseType(column.type)
  };
};

const parseType = type => {
  const typeCode = type.code;
  const typeString = typeCodes[typeCode];

  switch (typeString) {
    case 'list':
    case 'set':
      return parseListType(type);
    case 'tuple':
    case 'map':
      return parseMapType(type);
    case 'udt':
      return mapUdtType(type);
    default:
      return typeDescription[typeString];
  }
};

const parseListType = type => {
  return {
    type: 'array',
    items: parseType(type.info)
  };
};

const parseMapType = type => {
  return {
    type: 'array',
    items: type.info.map(t => parseType(t))
  };
};

const mapUdtType = type => {
  const columnFields = type.info.fields;
  const fieldEntries = columnFields.map(f => getFieldEntry(f));
  const fieldsData = Object.fromEntries(fieldEntries);

  return {
    type: 'object',
    properties: fieldsData
  };
};

const getFieldEntry = field => ([
  field.name,
  parseType(field.type)
]);

module.exports = parseColumn;

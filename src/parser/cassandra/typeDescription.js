module.exports = {
  "ascii": {"type": "string"},
  "bigint": {
    "type": "object",
    "properties": {
      "low": {"type": "number"},
      "high": {"type": "number"},
      "unsigned": {"type": "boolean"}
    }
  },
  "blob": {
    "type": "object",
    "properties": {
      "0": {"type": "number"},
      "1": {"type": "number"},
      "2": {"type": "number"},
      "3": {"type": "number"},
      "4": {"type": "number"},
      "5": {"type": "number"},
      "6": {"type": "number"},
      "7": {"type": "number"}
    }
  },
  "boolean": {"type": "boolean"},
  "date": {
    "type": "object",
    "properties": {
      "date": {"type": "object"},
      "_value": {"type": "object"},
      "year": {"type": "number"},
      "month": {"type": "number"},
      "day": {"type": "number"}
    }
  },
  "decimal": {
    "type": "object",
    "properties": {
      "_intVal": {
        "type": "object",
        "properties": {
          "bits_": {
            "type": "array",
            "items": {"type": "number"}
          },
          "sign_": {"type": "number"}
        }
      },
      "_scale": {"type": "number"}
    }
  },
  "double": {"type": "number"},
  "duration": {
    "type": "object",
    "properties": {
      "months": {"type": "number"},
      "days": {"type": "number"},
      "nanoseconds": {
        "type": "object",
        "properties": {
          "low": {"type": "number"},
          "high": {"type": "number"},
          "unsigned": {"type": "boolean"}
        }
      }
    }
  },
  "float": {"type": "number"},
  "inet": {
    "type": "object",
    "properties": {
      "buffer": {
        "type": "object",
        "properties": {
          "0": {"type": "number"},
          "1": {"type": "number"},
          "2": {"type": "number"},
          "3": {"type": "number"}
        }
      },
      "length": {"type": "number"},
      "version": {"type": "number"}
    }
  },
  "int": {"type": "number"},
  "smallint": {"type": "number"},
  "text": {"type": "string"},
  "time": {
    "type": "object",
    "properties": {
      "value": {
        "type": "object",
        "properties": {
          "low": {"type": "number"},
          "high": {"type": "number"},
          "unsigned": {"type": "boolean"}
        }
      },
      "_partsCache": {
        "type": "array",
        "items": {"type": "number"}
      },
      "hour": {"type": "number"},
      "minute": {"type": "number"},
      "second": {"type": "number"},
      "nanosecond": {"type": "number"}
    }
  },
  "timestamp": {"type": "object"},
  "timeuuid": {
    "type": "object",
    "properties": {
      "buffer": {
        "type": "object",
        "properties": {
          "0": {"type": "number"},
          "1": {"type": "number"},
          "2": {"type": "number"},
          "3": {"type": "number"},
          "4": {"type": "number"},
          "5": {"type": "number"},
          "6": {"type": "number"},
          "7": {"type": "number"},
          "8": {"type": "number"},
          "9": {"type": "number"},
          "10": {"type": "number"},
          "11": {"type": "number"},
          "12": {"type": "number"},
          "13": {"type": "number"},
          "14": {"type": "number"},
          "15": {"type": "number"}
        }
      }
    }
  },
  "tinyint": {"type": "number"},
  "uuid": {
    "type": "object",
    "properties": {
      "buffer": {
        "type": "object",
        "properties": {
          "0": {"type": "number"},
          "1": {"type": "number"},
          "2": {"type": "number"},
          "3": {"type": "number"},
          "4": {"type": "number"},
          "5": {"type": "number"},
          "6": {"type": "number"},
          "7": {"type": "number"},
          "8": {"type": "number"},
          "9": {"type": "number"},
          "10": {"type": "number"},
          "11": {"type": "number"},
          "12": {"type": "number"},
          "13": {"type": "number"},
          "14": {"type": "number"},
          "15": {"type": "number"}
        }
      }
    }
  },
  "varchar": {"type": "string"},
  "varint": {
    "type": "object",
    "properties": {
      "bits_": {
        "type": "array",
        "items": {"type": "number"}
      },
      "sign_": {"type": "number"}
    }
  }
};

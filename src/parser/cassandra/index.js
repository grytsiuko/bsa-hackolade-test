const cassandra = require('cassandra-driver');
const parseColumn = require('./parseColumn');

const parseCassandra = async config => {
  const client = getClient(config);
  await checkConnection(client);

  try {
    return await parseDataBaseSchema(client, config);
  } finally {
    await client.shutdown();
  }
};

const getClient = config => {
  const {host, port, user, password, localDataCenter} = config;
  const authProvider = new cassandra.auth.PlainTextAuthProvider(user, password);
  const contactPoints = [`${host}:${port}`];

  return new cassandra.Client({contactPoints, authProvider, localDataCenter});
};

const checkConnection = async client => {
  try {
    await client.connect();
  } catch (e) {
    throw new Error('Unable to connect to DB');
  }
};

const parseDataBaseSchema = async (client, config) => {
  const {keyspace} = config;
  const tables = await getTablesList(client, keyspace);
  const parsedTables = tables.map(t => parseTable(client, t));

  return {
    dataBaseName: keyspace,
    tables: await Promise.all(parsedTables)
  };
};

const getTablesList = async (client, keyspace) => {
  const query = 'SELECT * FROM system_schema.tables WHERE keyspace_name = ?;';
  const {rows} = await client.execute(query, [keyspace]);

  if (!rows || rows.length === 0) {
    throw new Error('Keyspace is empty or does not exist');
  }
  return rows;
};

const parseTable = async (client, table) => {
  const keyspaceName = table.keyspace_name;
  const tableName = table.table_name;

  const tableMetadata = await client.metadata.getTable(keyspaceName, tableName);
  const {columns} = tableMetadata;
  const parsedColumns = columns.map(c => parseColumn(c));
  await checkAllJsonColumns(client, table, parsedColumns);

  return {
    tableName: tableName,
    columnTypes: parsedColumns
  };
};

const checkAllJsonColumns = async (client, table, columns) => {
  const rows = await getRows(client, table);

  if (rows.length !== 0) {
    columns.forEach(c => checkJsonColumn(c, rows));
  }
};

const getRows = async (client, table) => {
  const keyspaceName = table.keyspace_name;
  const tableName = table.table_name;

  const query = `SELECT * from ${keyspaceName}.${tableName} LIMIT 1;`;
  const result = await client.execute(query);

  return result.rows;
};

const checkJsonColumn = (column, rows) => {
  if (column.description.type === 'string') {
    try {
      const value = rows[0][column.columnName];
      const json = JSON.parse(value);
      column.description = getObjectType(json);
    } catch (e) {
    }
  }
};

const getObjectType = object => {
  const type = typeof object;

  return type === 'object'
    ? getObjectFieldsTypes(object)
    : {type};
};

const getObjectFieldsTypes = object => {
  const result = {
    type: 'object',
    properties: {}
  };

  for (const [key, value] of Object.entries(object)) {
    result.properties[key] = getObjectType(value);
  }
  return result;
};

module.exports = parseCassandra;


const cassandra = require('cassandra-driver');

const testConfig = require('../testConfig');

const {host, port, user, password, localDataCenter} = testConfig;
const authProvider = new cassandra.auth.PlainTextAuthProvider(user, password);
const contactPoints = [`${host}:${port}`];

const getNewClient = () => new cassandra.Client({contactPoints, authProvider, localDataCenter});

const initDbAndTable = async () => {
  const client = getNewClient();
  await client.execute(
    `CREATE KEYSPACE IF NOT EXISTS ${testConfig.keyspace}
             WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1};`
  );
  await client.execute(
    `CREATE TYPE IF NOT EXISTS ${testConfig.keyspace}.grade
             (subject text, mark smallint);`
  );
  await client.execute(
    `CREATE TABLE IF NOT EXISTS ${testConfig.keyspace}.student
             (id int PRIMARY KEY, name text, grades list< frozen<grade> >);`
  );
  await client.shutdown();
};

const insertJsonIntoTable = async () => {
  const client = getNewClient();
  await client.execute(
    `INSERT INTO ${testConfig.keyspace}.student (id, name, grades) VALUES 
           (
              1, 
              '{"first_name": "Jack", "last_name": "Smith"}', 
              [{subject: 'Math', mark: 100}, {subject: 'History', mark: 95}]
           );`
  );
  await client.shutdown();
};

const truncateTable = async () => {
  const client = getNewClient();
  await client.execute(
    `TRUNCATE ${testConfig.keyspace}.student;`
  );
  await client.shutdown();
};

module.exports = {
  initDbAndTable,
  insertJsonIntoTable,
  truncateTable
};

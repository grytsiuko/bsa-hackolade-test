module.exports = {
  "host": "localhost",
  "port": "9042",
  "user": "cassandra",
  "password": "cassandra",
  "localDataCenter": "datacenter1",
  "keyspace": "tests",
  "dbms": "cassandra",
  "format": "jsonSchema4",
  "output": "./test/result.json"
};

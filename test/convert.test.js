const fs = require('fs');
const assert = require('assert');
const mocha = require('mocha');

const convert = require('../src/convert');
const testConfig = require('./testConfig');
const utils = require('./utils');

mocha.describe('Testing convert', () => {

  mocha.before(utils.initDbAndTable);

  mocha.afterEach(utils.truncateTable);

  mocha.it('should return correct schema', async () => {
    await assert.doesNotReject(async () => {
      await convert({...testConfig});
      const expected = fs.readFileSync('./test/json/basicCorrectResult.json');
      await fs.readFile(testConfig.output, 'utf-8', (err, data) => {
        assert.deepStrictEqual(JSON.parse(data), JSON.parse(expected.toString()));
      });
    });
  });

  mocha.it('should notice json in string column', async () => {
    await utils.insertJsonIntoTable();
    await assert.doesNotReject(async () => {
      await convert({...testConfig});
      const expected = fs.readFileSync('./test/json/jsonColumnCorrectResult.json');
      await fs.readFile(testConfig.output, 'utf-8', (err, data) => {
        assert.deepStrictEqual(JSON.parse(data), JSON.parse(expected.toString()));
      });
    });
  });
});


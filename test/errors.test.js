const assert = require('assert');
const mocha = require('mocha');

const convert = require('../src/convert');
const testConfig = require('./testConfig');
const utils = require('./utils');

mocha.describe('Testing errors', () => {

  mocha.before(utils.initDbAndTable);

  mocha.it('should return Unknown DBMS', async () => {
    await assert.rejects(
      async () => convert({...testConfig, dbms: 'abcdefg'}),
      {message:'Unknown DBMS'}
    );
  });

  mocha.it('should return Unable to connect to DB', async () => {
    await assert.rejects(
      async () => convert({...testConfig, port: '5555'}),
      {message:'Unable to connect to DB'}
    );
  });

  mocha.it('should return Illegal keyspace', async () => {
    await assert.rejects(
      async () => convert({...testConfig, keyspace: '........'}),
      {message:'Keyspace is empty or does not exist'}
    );
  });

  mocha.it('should return Unknown format', async () => {
    await assert.rejects(
      async () => convert({...testConfig, format: 'abcdefg'}),
      {message:'Unknown format'}
    );
  });

  mocha.it('should return Unable to write to file', async () => {
    await assert.rejects(
      async () => convert({...testConfig, output: undefined}),
      {message:'Unable to write to file'}
    );
  });
});

